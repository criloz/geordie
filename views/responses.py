from db import DB, EmailDB
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import tornado.template


@csrf_exempt
def Responses(request):
    """response page"""
    response = HttpResponse("Bad request")

    if request.method=="GET":
        ResponsesHandler.get_argument = request.GET.get
        response = ResponsesHandler.get()

    return response

class ResponsesHandler:
    @classmethod
    def get(self):
        #load couchdb
        DB.initialize()
        users = []
        for i in   EmailDB.get_email_by(EmailDB.GetResponses):
            users.append(i["value"])
        try:
            _file = open("resources/reports/responses.html")
            content = _file.read()
            _file.close()
        except:
            return HttpResponse("Internal server error")

        t = tornado.template.Template(content)

        return HttpResponse(t.generate(users= users))