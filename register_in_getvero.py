import logging
import re
from db import EmailDB,DB
from utils import Config
from getvero import GetVeroWeb
import uuid

""" script for register new users into getvero"""

class dummy_file:
    def __init__(self,content):
        self.content = content
        
    def read(self):
        return self.content
    
class CsvGetVeroSender:
    
    db_user = "GetveroNewUsersImporter"
    email_column = "customer_email"
    
    def __init__(self,getvero_settings,csv_settings):
        """ constructor """
        assert type(csv_settings)      == dict
        assert type(getvero_settings)  == dict
        
        self.export_properties_list = ["first_name","last_name","customer_email","hotel_name"]

        self.csv_separator          = csv_settings["separator"]
        
        self.log                    = logging.getLogger(self.__class__.__name__)
        #login in to getvero
        self.getvero_page           = GetVeroWeb(getvero_settings["user"],getvero_settings["password"])
        self.getvero_page.login()
        
        
    def prepare_csv(self,emails):
        """ create a csv file from list of couchdb email document"""
        self.log.info("Starting to export to csv file")
        titles = self.export_properties_list
        new_file = ""
        new_file = new_file + self.csv_separator.join(titles)
        new_file = new_file + "\n"     
        number_of_new_users = 0
        for email in emails:
            document         = email["value"] 
            
            #properties that will be printed in the cvs file
            export_properties = []
            for i in self.export_properties_list:
                export_properties.append(document[i].replace(self.csv_separator," "))

            new_file = new_file + self.csv_separator.join(export_properties)
            new_file = new_file + "\n"
            number_of_new_users = number_of_new_users +1
                
        self.log.info("CSV finished with %i users"%number_of_new_users)
        if number_of_new_users==0:
            return None
        return new_file
        
    
    def upload_unread_emails(self):
        """ get unread emails for the user x  and send as csv file"""
        emails = EmailDB.read_emails_by(EmailDB.Unread, self.db_user)
        
        self.upload_csv(emails)
        
        EmailDB.mark_emails_as_read_by(emails, EmailDB.Unread, self.db_user)
        
        self.log.info("unread emails for search new users uploaded to Getvero")
        
    
    def upload_csv(self,emails):
        """ import csv file to getver (see user interface)"""
            
        csv = {"content":dummy_file(self.prepare_csv(emails)),"name":str(uuid.uuid1())[0:10]+".csv"}
        
        if csv["content"].read()==None:
            self.log.info("Nothing for import")
            return
        self.getvero_page.import_users_form_csv(csv,self.email_column)
        
        self.log.info("new users imported to getvero")
            

if __name__ == "__main__":
    
    #load couchdb 
    DB.initialize()
    
    #load settings
    config = Config()
    
    getvero_settings = {
                      "user"       : config.getvero["user"]
                     ,"password"   : config.getvero["password"]
                     }
    

    csv_settings = {
                    "export_properties_list"  : config.csv["columns"]
                    ,"separator"              : config.csv["separator"]
    
    }
    logging.basicConfig(filename=config.log["file_path"],format = "<div class=\"%(levelname)s\">%(asctime)s:%(levelname)s:%(message)s</div>",level=logging.NOTSET)

    #load CsvEmailSender 
    sender  = CsvGetVeroSender(getvero_settings,csv_settings);
    
    
    #send unread
    print "uploading to getvero unread emails"
    sender.upload_unread_emails()
    
    
    print "Done !!!"