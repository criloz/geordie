import logging
import couchdb

class DBError(Exception):
    """ Database base exception """
    pass

class DB():
    """ base class for the database the app use couchdb (http://couchdb.apache.org/)"""
    
    couchdb = None
    log     = logging.getLogger("db.DB")
    dbs     = ["emails"]
    @classmethod
    def initialize(cls,*args,**kargs):
        """connect to couchdb, select email db if exist if not exist create it"""
        cls.couchdb = couchdb.Server(*args,**kargs)
        for i in cls.dbs:
            try:
                #create db for emails
                cls.couchdb.create(i)
            except:
                pass
    @classmethod
    def get_db(cls):
        """ select a db by name """
        return cls.couchdb[cls.dbname] 
    
    @classmethod
    def flush(cls):
        """ delete all the documents in the database """
        cls.couchdb.delete(cls.dbname)
        cls.couchdb.create(cls.dbname)
        cls.log.info("Email container flushed")
        pass