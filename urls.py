from django.conf.urls.defaults import patterns, include, url
from views.surveys import Surveys
from views.events import Events
from views.responses import Responses
from views.hotels import Hotels
from views.tripodvalidator import TripodValidator


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    # url(r'^server/', include('server.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r"^surveys/(.*)$", Surveys),
    url(r"^reports/events", Events),
    url(r"^reports/responses", Responses),
    url(r"^hotels/dictionary$", Hotels),
    url(r"^tripod_advisor/user/validator$", TripodValidator),
    url(r'^thanks$', 'django.views.generic.simple.direct_to_template', {'template': 'thanks/thanks.html'}),
)

