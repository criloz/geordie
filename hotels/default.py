import re
from email.parser import HeaderParser
from hotels.__utils__ import Hotel,EmailParserError
import time


#ignore this hotels
BadHotels = ["Villa del Palmar","Palmar Flamingos","VDP-PV","Marival"]

class default(Hotel):
    name = "default"
    email = "default"
    def parse_message(self,rep,data,index):
        """
        parse email message to python object
        """
        #get sections
        msg = data[0][1]
        header = HeaderParser().parsestr(data[0][1])
        #see if the email if a correct email
        msg   = msg.split("**Please DO NOT REPLY to this message. E-mail support@authorize.net if you have any questions.")
        if len(msg)<2:
            #this email does not have the correct email format
            raise EmailParserError("Bad email formated %s"%index)

        msg = msg[1]

        pre_sections = re.split("[=]+ (.*) [=]",msg)
        pre_sections.pop(0)

        sections = {}

        for i in range(0,len(pre_sections),2):
            sections[pre_sections[i].strip()] =  pre_sections[i+1]

        document = {}

        #get properties
        for i in sections:
            document[i] = {}
            elements   =  re.findall("(.*) : (.*)",sections[i])
            for j in elements:
                document[i][j[0].strip()] = j[1].strip()

        Plan = None
        Hotel = None

        #parse description
        #first case -> Villa del Palmar Internet Access - 1 Day - USD $11.90
        result = re.findall("(.*) Internet Access - ([0-9]+ Days?)",document["ORDER INFORMATION"]["Description"])
        if len(result)>0:
            Hotel = result[0][0].strip()
            Plan  = result[0][1].strip()

        if Plan==None or Hotel==None:
            #second case -> Avalon Grand Cancun - One Day Internet
            result = re.findall("(.*) - One Day Internet",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0].strip()
                Plan  = "1 Day"
        if Plan==None or Hotel==None:
            #third case -> MELIAMECABO-2
            result = re.findall("(.*)-([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1]
                if Plan == "1":
                    Plan = Plan + " Day"
                else:
                    Plan = Plan + " Days"
        if Plan==None or Hotel==None:
            #fourth case -> Baccara-Averatec1 Hotel Internet
            result = re.findall("(.*)([0-9]+) Hotel Internet",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1]
                if Plan == "1":
                    Plan = Plan + " Day"
                else:
                    Plan = Plan + " Days"
        if Plan==None or Hotel==None:
            #fifth case -> Palmar Flamingos Internet Access - 1 Hour - USD $7.25
            result = re.findall("(.*) Internet Access - ([0-9]+ Hours?)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1].strip()

        if Plan==None or Hotel==None:
            #sixth case -> ASISSENS-HP1
            result = re.findall("(.*)-HP([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1].strip()
                if Plan == "1":
                    Plan = Plan + " Hour"
                else:
                    Plan = Plan + " Hours"

        if Plan==None or Hotel==None:
            #seventh case -> Marival Internet Access - 30 Minutes - USD 6.90
            result = re.findall("(.*) Internet Access - ([0-9]+ Minutes?)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1].strip()

        if Plan==None or Hotel==None:
            # !!!!! for the future this is not the first case not try to merge with it
            #eight case -> Pelicanos - Internet Access 1 Day - USD $10.95)
            result = re.findall("(.*) - Internet Access ([0-9]+ Days?)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = result[0][0].strip()
                Plan  = result[0][1].strip()

        if Plan==None or Hotel==None:
            #nine case -> AvalonGrandNew
            if document["ORDER INFORMATION"]["Description"]=="AvalonGrandNew":
                Hotel = "Avalon Grand Cancun"
                Plan  = "1 Hour"
        if Plan==None or Hotel==None:
            #Ten case -> CROWNCAN2
            result = re.findall("CROWNCAN([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = "CROWNCAN"
                Plan  = result[0].strip() + " Hour"

        if Plan==None or Hotel==None:
            #11 case -> WESTINDELL3
            result = re.findall("WESTINDELL([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = "Westin"
                Plan  = result[0].strip()+ " Hour"

        if Plan==None or Hotel==None:
            #12 case -> MarivalDell2
            result = re.findall("MarivalDell([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = "Marival"
                Plan  = result[0].strip()+ " Hour"

        if Plan==None or Hotel==None:
            #13 case -> AVALONREEFDELL1
            result = re.findall("AVALONREEFDELL([0-9]+)",document["ORDER INFORMATION"]["Description"])
            if len(result)>0:
                Hotel = "Avalon Grand Cancun"
                Plan  = result[0].strip()+ " Hour"



        if Plan==None or Hotel==None:
            #raise exeption parsing document
            raise EmailParserError("not plan or hotel found in this email --->> Description section was(%s)"%document["ORDER INFORMATION"]["Description"])

        if Hotel in BadHotels:
            raise EmailParserError("we not work with this hotel")

        document["ORDER INFORMATION"]["Plan"] = Plan
        document["ORDER INFORMATION"]["Hotel"] = Hotel


        document["first_name"]     =  document["CUSTOMER BILLING INFORMATION"]["First Name"]
        document["last_name"]      =  document["CUSTOMER BILLING INFORMATION"]["Last Name"]
        document["customer_email"] =  document["CUSTOMER BILLING INFORMATION"]["E-Mail"]
        document["hotel_name"]     =  document["ORDER INFORMATION"]["Hotel"]
        document["hotel_email"]    =  "default"
        document["reg_date"]       =  time.mktime(time.gmtime())


        return document
