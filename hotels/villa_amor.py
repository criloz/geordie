__author__ = 'cristian'

from email.parser import HeaderParser
from hotels.__utils__ import Hotel
import re
import time
import email

def get_name(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.walk():
            if part.get_content_maintype() == 'text':
                text =  part.get_payload()
    elif maintype == 'text':
        text =  email_message_instance.get_payload()
    name = re.findall("Dear &nbsp;([^<>]+)",text)
    if name:
        return name[0]
    else:
        return None

class VillaAmor(Hotel):
    email  = "reservations@hotelvillaamorsayulita.com"
    name   = "Villa Amor"

    def __int__(self):
        pass

    def parse_message(self,rep,data,index):
        doc = {}
        header = HeaderParser().parsestr(data[0][1])
        msg = email.message_from_string(data[0][1])
        fields = re.findall('"\'?([^ ]*),? ([^ ]*).*\'?" <(.*)>',header['To'])
        if fields:
            if len(fields[0])==3:
                doc = {
                    "first_name"      : fields[0][0]
                    ,"last_name"      : fields[0][1]
                    ,"customer_email" : fields[0][2]
                    ,"hotel_name"     : self.name
                    ,"hotel_email"    : self.email
                    ,"reg_date"       : time.mktime(time.gmtime())
                }
        if len(doc)==0:
            name = get_name(data[0][1])
            if name is not None:
                doc = {
                    "first_name"      : name
                    ,"last_name"      : " "
                    ,"customer_email" : re.findall("<(.*)>",header["to"])[0]
                    ,"hotel_name"     : self.name
                    ,"hotel_email"    : self.email
                    ,"reg_date"       : time.mktime(time.gmtime())
                }

        return doc


