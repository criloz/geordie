__author__ = 'cristian'
from hotels.__utils__ import Hotel
import os
import inspect
import imp
hotels_by_email = {}

def load_modules():
    mods_path = os.path.abspath(os.path.dirname(__file__))
    for fname in os.listdir(mods_path):
        if not fname.startswith(('.', '_')) and fname.endswith('.py'):
            modname = os.path.splitext(fname)[0]
            try:
                mod = imp.load_source(modname,mods_path+"/"+modname+".py")
                for i in mod.__dict__:
                    if not inspect.isclass(mod.__dict__[i]):
                        continue
                    if i=="Hotel":
                        continue
                    if issubclass(mod.__dict__[i],Hotel):
                        hotels_by_email[mod.__dict__[i].email] = mod.__dict__[i]()
            except Exception as e:
                err = 'unable to load module {}: {}'
                print err.format(modname, e)


load_modules()