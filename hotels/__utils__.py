__author__ = 'cristian'

import logging

class Hotel:
    def __init__(self):
        self.log = logging.getLogger("Hotel."+self.__class__.__name__)

    pass

class EmailParserError(Exception):
    """Email parser base exception"""
    pass