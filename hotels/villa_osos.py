__author__ = 'cristian'

from email.parser import HeaderParser
from hotels.__utils__ import Hotel
import re
import time

class VillaAmor(Hotel):
    email  = "luisaviteri@gmail.com"
    name   = "Villa Osos"

    def __int__(self):
        pass

    def parse_message(self,rep,data,index):
        doc = {}
        header = HeaderParser().parsestr(data[0][1])
        fields = re.findall('"\'([^ ]*),? ([^ ]*).*\'" <(.*)>',header['To'])
        if fields:
            if len(fields[0])==3:
                doc = {
                    "first_name"      : fields[0][0]
                    ,"last_name"      : fields[0][1]
                    ,"customer_email" : fields[0][2]
                    ,"hotel_name"     : self.name
                    ,"hotel_email"    : self.email
                    ,"reg_date"       : time.mktime(time.gmtime())
                }
        return doc


