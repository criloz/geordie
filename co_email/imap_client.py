import imaplib
import logging
import traceback
from db import EmailDB,DB
from email.parser import HeaderParser
from hotels import hotels_by_email
from utils import Config
import time
from getvero import GetVeroEvents
import re

class EmailLogginError(Exception):
    pass

"""
    EmailScrapper class 
"""


class dummy_file:
    def __init__(self,content):
        self.content = content

    def read(self):
        return self.content

class ImapClient():
    
    def __init__(self,server,account,password):
        #create logger
        self.log = logging.getLogger("email."+self.__class__.__name__)
        #load the needed info for get logged into the server
        self.server        = server
        self.user          = account
        self.password      = password
        self.client        = None


    def connect(self):
        """
        connect to  imap service
        """
        try:
            mail = imaplib.IMAP4_SSL(self.server)
        except:
            error = traceback.format_exc()
            self.log.error("Could not connect to the server")
            self.log.debug(error)
            raise EmailLogginError("Could not connect to the server")
        try:
            mail.login(self.user, self.password)
        except:
            error = traceback.format_exc()
            self.log.error("Bad user or password")
            self.log.debug(error)
            raise EmailLogginError("Could not connect to the server")
        
        self.client = mail
        self.log.info("connected to the imap server")
        
    def logout(self):
        self.client.close()

    def store_emails(self,search="UNSEEN",flag="\SEEN"):
        """
           process  email, after that, if the operation was successfully mark as a read email
           
           return a list with the emails parsed  
        """
        self.connect()

        self.log.info("Starting to parser unread emails")

        #select inbox
        self.client.select("Inbox",readonly=False)
        #select not read emails   
        
        unseen = self.client.search(None,search)[1][0].split(" ")
        self.log.error("unseen------>>" + str(len(unseen)))
        for i in unseen:
            try:
                resp, data = self.client.fetch(i,"(RFC822)")
            except:
                self.log.error("Error getting email message")
                self.log.error(traceback.format_exc())
                continue
            try:
                #parse header
                header = HeaderParser().parsestr(data[0][1])
                #parse email
                email_from =  re.findall("<([^ ]+)>",header["from"])[0]
                doc = hotels_by_email.get(email_from,hotels_by_email["default"]).parse_message(resp,data,i)
                #save using the key E-Mail as id
                self.log.error([doc.keys(),email_from])
                email =  doc["customer_email"]
                if email == "":
                    self.log.warn("skipping item because not have customer client email.")
                    #mark as read
                    try:
                        self.client.store(i,'+FLAGS',flag)
                    except:

                        self.log.error("Error setting the email as read, email  will be ignored")
                        self.log.error(traceback.format_exc())
                    continue
                hotel =  doc["hotel_name"].lower().replace(" ","")
                _id = hotel + "_" + email
                doc["_id"] = _id
                #save the email
                EmailDB.write(doc, data[0][1])

                #mark as read
                try:
                    self.client.store(i,'+FLAGS',flag)
                except:

                    self.log.error("Error setting the email as read, email  will be ignored")
                    self.log.error(traceback.format_exc())
                    continue

            except  Exception, e:
                if e.message=="we not work with this hotel":
                    try:
                        self.client.store(i,'+FLAGS',flag)
                    except:
                        self.log.error("Error setting the email as read, email  will be ignored")
                        self.log.error(traceback.format_exc())
                        continue
                    continue
                self.log.error("Error parsing email")
                self.log.error(traceback.format_exc())
        
        self.log.info("Unread emails parsed")
        self.logout()


if __name__=="__main__":
    import logging
    logging.basicConfig()
    config = Config()
    imapReader = ImapClient(config.imap["server"],config.imap["account"],config.imap["password"])
    imapReader.store_emails()