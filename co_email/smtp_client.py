import logging
import traceback
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

class SmtpLogginError(Exception):
    pass


class SmtpClient():
    """
        SmtpClient
    """
    def __init__(self,server,account,password,**kargs):
        """ constructor """
        if server==None:
            server = kargs["server"]
        if account==None:
            account = kargs["account"]
        if password==None:
            password = kargs["password"]
        #create logger
        self.log = logging.getLogger("email."+self.__class__.__name__)
        #load the needed info for get logged into the server
        self.server        = server
        self.user          = account
        self.password      = password
        self.client        = None
        
    def connect(self):
        """
        connect to the smtp service
        """
        try:
            smtp = smtplib.SMTP(self.server)
        except:
            error = traceback.format_exc()
            self.log.error("Could not connect to the server")
            self.log.debug(error)
            raise SmtpLogginError("Could not connect to the server")
        try:
            smtp.login(self.user, self.password)
        except:
            error = traceback.format_exc()
            self.log.error("Bad user or password")
            self.log.debug(error)
            raise SmtpLogginError("Could not connect to the server")
        
        self.client = smtp
        self.log.info("connected to the Smtp server")

    def logout(self):
        self.client.close()
    
    def send_mail(self, send_to, subject, text, files=[]):
        """ send a email using smtp service """
        assert type(send_to)==list
        assert type(files)==list
        
        
        msg = MIMEMultipart()
        msg['From']    = self.user
        msg['To']      = COMMASPACE.join(send_to)
        msg['Date']    = formatdate(localtime=True)
        msg['Subject'] = subject
        
        msg.attach( MIMEText(text) )
        
        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(f["content"])
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition', 'attachment; filename="%s"' % f["file_name"])
            msg.attach(part)
       
        self.connect()
        self.client.sendmail(self.user, send_to, msg.as_string())
        self.logout()