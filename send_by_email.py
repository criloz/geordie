import logging
import re
from db import EmailDB,DB
from co_email import SmtpClient
from utils import Config
import time
import os
from getvero import GetVeroEvents
from hotels import hotels_by_email
"""
    script, obtains all the new email parsed to the db, dump them to csv file and send them to email inbox that have in the config file
"""

class CsvEmailSender:
    """
        
    """
    
    def __init__(self,users,smtp_settings,csv_settings):
        """ contructor"""
        assert type(smtp_settings) == dict
        assert type(users)         == list
        assert type(csv_settings)  == dict
        
        """ initialize smtp service, and load configurations"""
        self.smtp_client = SmtpClient(**smtp_settings)
        self.export_properties_list =  ["first_name","last_name","customer_email","hotel_name"]
        self.csv_separator          = csv_settings["separator"]
        self.users                  = users
        self.log                    = logging.getLogger(self.__class__.__name__)
        
    def prepare_csv(self,emails):
        """ create a csv file from list of couchdb email document"""
        self.log.info("Starting to export to csv file")
        titles = self.export_properties_list
        new_file = ""
        new_file = new_file + self.csv_separator.join(titles)
        new_file = new_file + "\n"     
       
        for email in emails:
            document         = email["value"] 
            document["uuid"] = email["key"]

            #properties that will be printed in the cvs file
            export_properties = []
            for i in self.export_properties_list:
                export_properties.append(document[i].replace(self.csv_separator," "))
                
            new_file = new_file + self.csv_separator.join(export_properties)
            new_file = new_file + "\n"
                
            self.log.info("Export process finished")

        return new_file
        
    
    def send_unread_emails(self,message):
        """ get unread emails for the user x  and send as csv file"""
        for user in self.users:
            emails = EmailDB.read_emails_by(EmailDB.Unread, user["name"])
            
            if len(emails)==0:
                continue
            
            self.send_csv_file(emails, message,user)
            
            EmailDB.mark_emails_as_read_by(emails, EmailDB.Unread, user["name"])
            
            self.log.info("unread emails of duplicates sended to %s - %s"%(user["name"],user["email"]))
    
    def send_unread_emails_of_duplicates(self,message):
        """ get unread emails for the user x  and send as csv file"""
        for user in self.users:
            emails = EmailDB.read_emails_by(EmailDB.UnreadDuplicates, user["name"])
            
            if len(emails)==0:
                continue
            
            self.send_csv_file(emails, message,user)
            
            EmailDB.mark_emails_as_read_by(emails, EmailDB.UnreadDuplicates, user["name"])
            
            self.log.info("unread emails of duplicates sended to %s - %s"%(user["name"],user["email"]))
        
    
    def send_csv_file(self,emails,message,user):
            """ use smtp services for send the email"""
            assert type(message) == dict
            
            csv  = self.prepare_csv(emails)
                
            files = [{"file_name":message["csv_file_name"],"content":csv}]
            
            self.smtp_client.send_mail([user["email"]], message["subject"], message["text"], files)
            
            self.log.info("Email sent")
            

if __name__ == "__main__":
    
    dateUnformat =  time.ctime()
    dateFormat = dateUnformat.replace(" ","_").replace(":","_")
    
    #load couchdb 
    DB.initialize()
    
    #load settings
    config = Config()
    
    smtp_settings = {
                     "server"     : config.smtp["server"]
                     ,"account"   : config.smtp["account"]
                     ,"password"  : config.smtp["password"]
                     }
    #emails targets  inboxes
    readers = config.readers
      
    csv_settings = {
                    "export_properties_list"  : config.csv["columns"]
                    ,"separator"              : config.csv["separator"]
    
    }
    
    logging.basicConfig(filename=config.log["file_path"],format = "<div class=\"%(levelname)s\">%(asctime)s:%(levelname)s:%(message)s</div>",level=logging.NOTSET)


    #load CsvEmailSender 
    sender  = CsvEmailSender(readers,smtp_settings,csv_settings);
    
    unread_message = { "text"         : "csv for new transactions, already imported to GetVero!!"
                      ,"subject"      : "csv for new transactions at %s"%dateUnformat
                      ,"csv_file_name": "NewTransactions_%s.csv"%dateFormat
                    }
    #send unread
    print "sending unread emails to the readers"
    sender.send_unread_emails(unread_message)
    
    unread_duplicates_message = { "text"          : "duplicates transactions"
                                 ,"subject"       : "Duplicates transactions at %s"%dateUnformat
                                 ,"csv_file_name" : "DuplicatesTransactions_%s.csv"%dateFormat
                                }
    # send unread duplicates
    print "sending unread emails of duplicates to the readers"
    sender.send_unread_emails_of_duplicates(unread_duplicates_message)
    
    # send reports
    """ convert into pdf the /reports/event page"""
    print "send reports"
    link = "http://54.243.212.67/reports/events"
    _file = {"file_name":"report_%s.pdf"%dateFormat}
    _file["content"] = os.popen("wkhtmltopdf %s -"%link).read()
    for user in readers:
        sender.smtp_client.send_mail([user["email"]], "event report", "last report", [_file])
    print "Done !!!"


    # check waht hotel still not send email to the authnet inbox
    getveroClient = GetVeroEvents(config.getvero["auth_token"],config.getvero["dev_mode"])

    for hotel_email in hotels_by_email:
        emails_coming = False
        for element in   EmailDB.get_email_by(EmailDB.TransactionsByHotelEmail,include_docs=True,descending=True,limit=1,endkey=[hotel_email,time.mktime(time.gmtime())-3*24*60*60],startkey=[hotel_email,{}]):
            emails_coming = True
        if not emails_coming:
            getveroClient.track_user(hotel_email,  "emails_are_not_coming",{})