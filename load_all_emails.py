"""
    load unread and read emails from the imap client  to the db
"""


from co_email import  ImapClient
from db import DB
from utils import Config
import logging

if __name__ == "__main__":
    DB.initialize()
    config = Config()
    logging.basicConfig(filename=config.log["file_path"],format = "<div class=\"%(levelname)s\">%(asctime)s:%(levelname)s:%(message)s</div>",level=logging.NOTSET)
    #read and save the emails in the db
    imapReader = ImapClient(config.imap["server"],config.imap["account"],config.imap["password"])
    imapReader.store_emails('(SEEN FROM "%s")'%("reservations@hotelvillaamorsayulita.com"))
    imapReader.store_emails()