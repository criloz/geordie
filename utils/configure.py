import json

class Config(object):
    def __init__(self):
        try:
            _file = open("resources/config.ini","r")
        except:
            _file = open("../resources/config.ini","r")
        content = _file.read()
        _file.close()
        doc = json.loads(content)
        for i in doc:
            self.__setattr__(i,doc[i])
        
    def save(self):
        doc = self.__dict__
        _file = open("resources/config.ini","w+")
        _file.write(json.dumps(doc))
        _file.close()
