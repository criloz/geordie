"""" Load only unread emails from the imap client to the db """

from co_email import  ImapClient
from db import DB
from utils import Config
import logging

if __name__ == "__main__":
    DB.initialize()
    config = Config()
    logging.basicConfig(level=logging.NOTSET)
    #read and save the emails in the db
    imapReader = ImapClient(config.imap["server"],config.imap["account"],config.imap["password"]) 
    imapReader.store_emails()
    
    