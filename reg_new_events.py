""" Create a event on getvero (visit hotel) when a new user is register on the db"""

import logging
from db import EmailDB,DB
from utils import Config
from getvero import GetVeroEvents

class GetVeroEventsCreator:
    
    db_user = "GetveroNewEvents"
    
    def __init__(self,getvero_settings):
        """ contructor """
        assert type(getvero_settings)  == dict
        
        
        self.log                    = logging.getLogger(self.__class__.__name__)
        
        #getvero api
        self.getvero_events         = GetVeroEvents(getvero_settings["auth_token"],getvero_settings["dev_mode"])
        
        self.visit_event            =  getvero_settings["visit_event"]
        
        
        
    
    def launch_new_vist_event(self):
        """ get unread emails for the user x  and send as csv file"""
        emails = EmailDB.read_emails_by(EmailDB.Unread, self.db_user)
        
        for i in emails:
            doc =  i["value"]
            res = EmailDB.get_email_by(EmailDB.UUID,key=i["key"],include_docs=True)
            couchdoc = None
            for j in res:
                couchdoc = j

            email = doc["customer_email"].lower()
            
            hotel = doc["hotel_name"]

            hotel_email =  doc["hotel_email"]

            data = {"hotel":hotel,"uuid":i["key"]}
            
            try:
                if hotel_email=="default":
                    self.getvero_events.track_user(email,  self.visit_event,data)
                else:
                    self.getvero_events.track_user(email,  "visits_"+hotel.lower().replace(" ","_"),data)
                event = "Email sent"
                couchdoc["events"] = couchdoc.get("events",{})
                event_count   = couchdoc["events"].get(event,0)
                couchdoc["events"][event] = event_count + 1
                EmailDB.update([couchdoc])
            except:
                pass
            
        EmailDB.mark_emails_as_read_by(emails, EmailDB.Unread, self.db_user)
        
        self.log.info("%i events logged into getvero"%(len(emails)))
        

if __name__ == "__main__":
    
    #load couchdb 
    DB.initialize()

    #load settings
    config = Config()
    
    getvero_settings = {
                     "auth_token"   : config.getvero["auth_token"]
                     ,"dev_mode"    : config.getvero.get("dev_mode",True)
                     ,"visit_event" : config.getvero["visit_event"]
                     }
    

    csv_settings = {
                    "export_properties_list"  : config.csv["columns"]
                    ,"separator"              : config.csv["separator"]
    
    }
    
    logging.basicConfig(filename=config.log["file_path"],format = "<div class=\"%(levelname)s\">%(asctime)s:%(levelname)s:%(message)s</div>",level=logging.NOTSET)

    #load CsvEmailSender 
    sender  = GetVeroEventsCreator(getvero_settings);
    
    
    #send unread
    print "uploading to getvero new  events"
    sender.launch_new_vist_event()
    
    
    print "Done !!!"
